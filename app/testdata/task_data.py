# -*- coding: utf-8 -*-

from app.testdata.base_data import BASE_TASK_URL

# 任务列表
TASK_LIST_DATA = {
    "url": BASE_TASK_URL + "/task/list",
    "method": "GET",
    "data": None
}

# 领取任务
TASK_ADD_DATA = {
    "url": BASE_TASK_URL + "/task/add",
    "method": "POST",
    "data": {
        "task_id": 0
    }
}
