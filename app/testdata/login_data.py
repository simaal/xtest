# -*- coding: utf-8 -*-

from app.testdata.base_data import BASE_LOGIN_URL

# 登录
LOGIN_SUCCESS_DATA = {
    "url": BASE_LOGIN_URL + "/login",
    "method": "POST",
    "data": {
        "username": "admin",
        "password": "123456"
    },
    "headers": {
        "source": "sample"
    }
}

LOGIN_ERROR_DATA = {
    "url": BASE_LOGIN_URL + "/login",
    "method": "POST",
    "data": {
        "username": "admin",
        "password": "123456789"
    },
    "headers": {
        "source": "sample"
    }
}