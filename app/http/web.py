# -*- coding: utf-8 -*-

import sys
import os

# python命令执行处理
curPath = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)

from flask import Flask, request, abort

from app.http.output import Output

app = Flask(__name__)

TOKEN = "123456789"


@app.route('/')
def home():
    return 'Welcome To Sample!'


@app.route('/login/', methods=['POST'])
def login():
    """登录"""

    username = request.form['username']
    password = request.form['password']

    if username == "admin" and password == "123456":
        data = {"username": username, "token": TOKEN}
        return Output().success(data)
    else:
        abort(401)


@app.route('/task/list')
def task_list():
    """任务列表"""
    token = request.headers.get('token', None)
    if token != TOKEN:
        abort(401)

    data = [
        {
            "id": 1,
            "name": "task one",
            "time": "2020-04-01"
        },
        {
            "id": 2,
            "name": "task two",
            "time": "2020-04-02"
        },
        {
            "id": 3,
            "name": "task three",
            "time": "2020-04-03"
        },
    ]
    return Output().success(data)


@app.route('/task/add', methods=['POST'])
def task_add():
    """添加任务"""

    token = request.headers.get('token', None)
    if token != TOKEN:
        abort(401)

    task_id = int(request.form['task_id'])
    if task_id not in (1, 2, 3):
        return Output().message("task_id is error").error()
    return Output().success()


@app.errorhandler(401)
def auth_fail(error):
    return Output().code(401).message("auth fail!").error()


@app.errorhandler(404)
def not_found(error):
    return Output().code(404).message("page not found").error()


if __name__ == '__main__':
    """
    http://127.0.0.1:5000/
    """
    app.run()
