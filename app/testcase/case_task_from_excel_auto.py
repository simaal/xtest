# -*- coding: utf-8 -*-

import unittest

from ddt import ddt, data

from app.lib.excel.excel import Excel
from app.auto_case import AutoCase


@ddt
class CaseTaskFromExcelAuto(AutoCase):
    """
    任务测试-通过excel获取数据进行接口测试--全封装自动化

    约定
    一个excel为：excel_data
    excel中的一个sheet为：sheet_data
    sheet中的一个case为：case_data
    """

    sheet_data = Excel("data_example.xls").get_sheet_data("Sheet1", 1, 2)
    excel_data = Excel("data_example.xls").get_excel_data(1, 2)

    def __init__(self, methodName="runTest"):
        super().__init__(methodName)

    @data(*sheet_data)
    def test_excel_single_sheet(self, case_data):
        """
        读取指定sheet做测试

        因为做了excel模板规则和代码封装，针对每个文件只需要读取执行即可
        使用者只需要实现一个文件读取，和一个test_*类即可
        """

        self.run_case_test(case_data)

    @data(*excel_data)
    def test_excel_all_sheet(self, sheet_data):
        """
        对excel所有sheet做测试

        因为做了excel模板规则和代码封装，针对每个文件只需要读取执行即可
        使用者只需要实现一个文件读取，和一个test_*类即可
        """

        for case_data in sheet_data:
            self.run_case_test(case_data)


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(CaseTaskFromExcelAuto))
    unittest.TextTestRunner(verbosity=2).run(suite)
