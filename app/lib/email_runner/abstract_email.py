# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod

from app.core.config import Config
from app.core.mail import Mail


class AbstractEmail(metaclass=ABCMeta):
    """
    邮件抽象类，方便实现不同的text和html、file
    """

    def __init__(self, subject="", msg_from="", msg_to=""):
        """
        :param subject: string 邮件标题
        :param msg_from: string 发送者
        :param msg_to: string 接受者
        """
        self.message = None
        self.mail = Mail()

        config = Config.init()

        self._msg_from = msg_from if msg_from else config.get("mail", "default_from")
        self._msg_to = msg_to if msg_to else config.get("mail", "default_to")
        self._msg_subject = subject if subject else config.get("mail", "default_subject")

    @abstractmethod
    def set_message(self, msg, msg_type="plain"):
        """设置消息"""
        pass

    def sendmail(self, mail_options=(), rcpt_options=()):
        """发送邮件"""
        return self.mail.sendmail(self.message, mail_options, rcpt_options)
