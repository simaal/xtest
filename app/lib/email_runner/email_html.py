# -*- coding: utf-8 -*-

from email.header import Header
from email.mime.text import MIMEText

from app.lib.email_runner.abstract_email import AbstractEmail


class EmailHtml(AbstractEmail):
    """
    发送纯html邮件邮件
    """

    def __init__(self, subject="", msg_from="", msg_to=""):
        super().__init__(subject, msg_from, msg_to)

    def set_message(self, msg, msg_type="html"):
        """
        设置消息
        :param msg: string 消息
        :param msg_type: string 消息类型
        :return:
        """

        message = MIMEText(msg, 'html', 'utf-8')
        message['From'] = Header(self._msg_from, 'utf-8')  # 发送者
        message['To'] = Header(self._msg_to, 'utf-8')  # 接收者

        message['Subject'] = Header(self._msg_subject, 'utf-8')  # 邮件标题

        self.message = message
