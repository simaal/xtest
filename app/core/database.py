# -*- coding: utf-8 -*-

import threading
import pymysql as pymysql

from app.core.config import Config
from app.core.log import Log


class Database:
    """
    配置文件操作类
    使用 db = Database()
    """

    _instance_lock = threading.Lock()

    def __init__(self):
        self._log = Log.init()
        self._config = Config.init()

        self._host = self._config.get("mysql", "host")
        self._username = self._config.get("mysql", "username")
        self._password = self._config.get("mysql", "password")
        self._port = self._config.getint("mysql", "port")
        self._database = self._config.get("mysql", "database")

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            with cls._instance_lock:
                if not hasattr(cls, '_instance'):
                    cls._instance = object.__new__(cls)

                    cls.conn, cls.cursor = cls()._get_database()

        return cls._instance

    def _get_database(self):
        """获取pymysql对象实例"""

        conn = pymysql.connect(host=self._host, port=self._port, user=self._username, password=self._password,
                               database=self._database)

        cursor = conn.cursor()

        return conn, cursor

    def __del__(self):
        self.conn.close()
        self._log.info("mysql is closed!")
