# -*- coding: utf-8 -*-

from app.core.database import Database
import unittest


class TestEmail(unittest.TestCase):
    """
    邮件测试
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.db = Database()

    def test_mysql(self):
        """测试mysql查询"""

        sql = "select * from kc_topic limit 10"
        self.db.cursor.execute(sql)
        # 使用 fetchone() 方法获取单条数据.
        data = self.db.cursor.fetchone()

        print(data)


if __name__ == "__main__":
    # verbosity=*：默认是1；设为0，则不输出每一个用例的执行结果；2-输出详细的执行结果
    unittest.main(verbosity=2)
