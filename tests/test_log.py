# -*- coding: utf-8 -*-

import unittest

from app.core.log import Log


class TestLog(unittest.TestCase):
    """
    测试日志文件
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.log = Log.init()

    def test_log(self):
        self.log.log(20, "i am default")
        self.log.debug("i am debug")
        self.log.warning("i am warn")
        self.assertTrue(True)


if __name__ == "__main__":
    unittest.main(verbosity=2)
