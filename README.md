# xtest

## 介绍

基于python3的单元测试框架

只是对最基本的库做了简单封装，最大化保留了原生库功能，基于框架可快速的进行接口自动化测试。

## 使用说明

### 框架文件结构

    app/ 应用
        core/ 核心类
            config.py 配置
            database.py 数据库
            log.py 日志
            mail.py 邮件
        helper/ 帮助类
            const.py 常量
            helper.py 帮助类
        http/ 简单的flask web
            output.py 简单的输出类
            web.py 启动web测试服务
        lib/ 扩展库
            email_runner/ 邮件扩展
            excel/ excel操作扩展
            html_test_runner/ html test runner
        testcase/ 测试用例
            case_login.py 登录测试
            case_task.py  任务测试
            case_task_from_excel.py  从excel获取数据进行任务测试
            case_task_from_excel_auto.py 从excel获取数据进行任务自动化测试
        testdata/ 测试用例数据
        auto_case.py 自动case父类
        base_app.py app顶层类
        base_case.py 测试用例父类
    config/ 配置文件
        app.ini 应用配置
        database.ini 数据库配置
        mail.ini 邮件配置
    public/ 入口执行
        main.py 入口执行
    storage/ 文件存储
        data/ 数据文件
        logs/ 日志文件
        report/ 测试报告
    tests/ 框架单元测试
    README.md 文档
    requirements.txt venv请求库列表

### 框架图

![框架图](https://images.gitee.com/uploads/images/2021/0430/114947_d0e6d61e_75408.png "frame.png")

## 如何使用

### 快速尝试

框架中做了一些实例，如果需要快速尝试，可以启动web服务，执行测试用例，查看一些效果。

    1.进入config文件夹，修改配置文件
    复制app.ini.example为app.ini
    复制database.ini.example为database.ini
    复制mail.ini.example为mail.ini

    PS：默认邮件发送为debug模式，不会发送到邮箱，如需发送邮件，需要进行以下修改
    修改mail.ini文件中的debug=0，并且修改其他邮件参数

    2. 运行测试web服务
    进入app/http文件夹，执行命令 python web.py
    
    PS:默认访问为http://127.0.0.1:5000

    3. 进入public文件夹，执行命令 python main.py

执行以上步骤后，就会把框架中涉及到的所有接口测试用例跑一遍，然后`storage/report`文件夹中会有一个report开头的测试报告html文件。

### 编写接口测试

因为框架对一些底层依赖做了简单封装，就不需要对这些依赖做任何处理，只需要按照框架的方式进行接口测试代码书写接口。

在app/testcase书写你的接口测试 ，采用unittest原生写法，你只需要学习unittest扩展功能既可。

#### 方式一：纯代码编写

这种是最原生的unittest接口自动化方式，所有内容通过书写unittest代码进行。

> 详情见`app/testcase`文件夹中的`case_login.py`和`case_task.py`文件

#### 方式二：通过excel进行测试

这种把相关数据存储到excel，然后从excel读取数据进行单元测试，需要编写对excel处理的代码。

> 详情见`app/testcase`文件夹中的`case_task_from_excel.py文件

#### 方式三：纯自动化excel测试

框架中做了一个简单封装和excel定义，我们所有内容都在excel进行书写，然后写一个简单的unittest文件即可。

    sheet_data = Excel("data_example.xls").get_sheet_data("Sheet1", 1, 2)

    @data(*sheet_data)
    def test_excel_single_sheet(self, case_data):
        self.run_case_test(case_data)

> 详情见`app/testcase`文件夹中的`case_task_from_excel_auto.py文件

#### 备注

无论采用哪种方式，xtest框架只是做了一些实例，给大家做参考，并不是完全的标准，每个业务都应该根据自己的情况做修改调整。

比如：第三种方式可以更高层级的封装，这样做接口自动化只需要写excel，不需要写任何代码。

**适合自己的才是最好的。**

### unittest注意事项

**上下文处理**

unittest中集成了一个父类BaseCase，在unittext直接使用`self.context`就可以进行上下文内容操作。

**接口流程测试**

通常我们要测试一个流程，那么在unittext中执行单元测试是无序的，这个时候可以对测试方法进行顺序命名，比如：

    test_a_login(self)

    test_b_get_user_info(self)

这个时候就会按照test_a_*--》test_b_*的顺序执行

## 框架功能

### 配置

配置文件目录是固定的，支持多个配置文件，直接在config文件夹中添加.ini文件即可。

[Config文档](https://docs.python.org/zh-cn/3/library/configparser.html)

    from core.config import Config

    config = Config.init()
    config.get("app","app_name")

### 日志

日志封装比较简单，只提供了基础的。

[Log文档](https://docs.python.org/zh-cn/3/library/logging.html)

    from core.log import Log

    log log = Log.init()
    log.debug("i am default info")

### HTMLTestRunner

HTMLTestRunner,github上面一个支持python3的项目，存在报错，对核心代码做了一点修改。

[HTMLTestRunner文档](https://github.com/findyou/HTMLTestRunnerCN/tree/dev)

    from app.lib.html_test_runner import HTMLTestRunnerCN

    with open(report_filename, "wb") as f:
            runner = HTMLTestRunnerCN.HTMLTestReportCN(stream=f, title='XX接口自动化测试报告',description='环境：mac 浏览器：chrome')
            runner.run(testcases)

### Email

[email文档](https://docs.python.org/3/library/email.examples.html)

    from app.lib.email_runner.email_runner import EmailRunner

    r = self.email_runner.make_html("富文本邮件", "html", "测试html")
    r.set_message("<p>富文本邮件</p>我是换行内容")
    result = r.sendmail()

### Excel

只对excel读操作做了一个简单封装，可以自行对写操作做封装。

[xlrd文档](https://xlrd.readthedocs.io/en/latest/)

    from app.lib.excel.excel import Excel

    e = Excel("data_中文演示.xls")
    sheet_data = e.get_sheet_data("测试1", 1, 2)
    # 返回的数据格式为[{"name":value,"name1":"value1"},{"name":"value3","name1":"value4"}]

    e = Excel("data_example.xls")
    excel_data = Excel("data_example.xls").get_excel_data(1, 2)

## 参考

这个框架只是做了一些封装和一些实例，所有的思想都来自于一些书籍和文章。

特此鸣谢以下两篇博文：

- https://www.cnblogs.com/yinjia/p/9503408.html
- https://blog.csdn.net/songlh1234/article/details/84317617

参考书籍较多，这里不做展示

## 后续

我不是专业的测试，封装此框架的目的是给公司相关测试同事使用，只是在这个过程中对接口自动化测试做了一些学习，并开发出这个框架。

若有更好的建议和意见，请留言评论！